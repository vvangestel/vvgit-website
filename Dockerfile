FROM docker.io/library/node:16-alpine as builder

RUN mkdir -p /website/scripts /website/src
WORKDIR /website

COPY package.json /website
COPY scripts /website/scripts
COPY src /website/src

RUN npm install
RUN npm run build

FROM docker.io/library/node:16-alpine

RUN mkdir -p /website/scripts /website/src
WORKDIR /website

COPY package.json /website
COPY scripts /website/scripts
COPY --from=builder /website/dist /website/dist

RUN npm install --only=prod

CMD ["npm", "run", "production"]
